interface GridRulesProcessor {
  Grid processRules(Grid previous);
}
