Grid grid;

// ===== RENDER =====

void setup() {
  size(300, 300);
  background(255);
  grid = new Grid(width/Cell.SIZE, width/Cell.SIZE);
}

void draw() {
  for (int y = 0; y < grid.gridHeight; y++) {
    for (int x = 0; x < grid.gridWidth; x++) {
      grid.getCell(x, y).render();
    }
  }
}

// ===== EVENTS =====

void keyPressed() {
  if (key == 's') {
    GridRulesProcessor processor = new ClearGridProcessor(); // Replace Rules Processor HERE
    grid = processor.processRules(grid);
  }
}

void mousePressed() {
  int xCoords = mouseX/Cell.SIZE;
  int yCoords = mouseY/Cell.SIZE;

  grid.toggle(xCoords, yCoords);
}

void mouseDragged() {
  int xCoords = mouseX/Cell.SIZE;
  int yCoords = mouseY/Cell.SIZE;
  int pxCoords = pmouseX/Cell.SIZE;
  int pyCoords = pmouseY/Cell.SIZE;

  if (xCoords != pxCoords || yCoords != pyCoords) {
    grid.toggle(xCoords, yCoords);
  }
}
