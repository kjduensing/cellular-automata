class Cell {
  static final int SIZE = 30;
  boolean isOn = false;
  int x, y = 0;
  int size = 0;
  
  Cell(int xCoords, int yCoords) {
    this.x = xCoords;
    this.y = yCoords;
  }
  
  void toggle() {
    this.isOn = !this.isOn;
  }
  
  void turnOff() {
    this.isOn = false;
  }
  
  void turnOn() {
    this.isOn = true;
  }
  
  void render() {
    stroke(100);
    
    if (this.isOn) {
      fill(0);
    } else {
      fill(255);
    }
    
    rect(this.x, this.y, SIZE, SIZE);
  }
}
