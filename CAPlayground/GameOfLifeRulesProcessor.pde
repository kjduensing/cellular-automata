class GameOfLifeRulesProcessor implements GridRulesProcessor {
  Grid processRules(Grid previous) {
    Grid next = new Grid(width/Cell.SIZE, height/Cell.SIZE);
    
    // Rule 1: If currently on and < 2 living neighbors, next state is off (underpopulated)
    // Rule 2: If currently on and 2 or 3 living neighbors, next state is on (stable)
    // Rule 3: If currently on and > 3 living neighbors, next state is off (overpopulated)
    // Rule 4: If currently off and exactly 3 living neighbors, next state is on (thriving)
    
    return next;
  }
}
