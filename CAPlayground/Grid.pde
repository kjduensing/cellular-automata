class Grid {
  private Cell[][] grid;
  int gridWidth, gridHeight;
  
  Grid(int w, int h) {
    gridWidth = w;
    gridHeight = h;
    
    grid = new Cell[w][h];
    
    for (int y = 0; y < w; y++) {
      for (int x = 0; x < h; x++) {
        grid[x][y] = new Cell(x * Cell.SIZE, y * Cell.SIZE);
      }
    }
  }
  
  void toggle(int x, int y) {
    if ((x < this.grid.length && x >= 0) && (y < this.grid[0].length && y >= 0)) {
      this.grid[x][y].toggle();
    }
  }
  
  Cell getCell(int x, int y) {
    return this.grid[x][y];
  }
  
  void setCell(int x, int y) {
      this.grid[x][y] = new Cell(x * Cell.SIZE, y * Cell.SIZE);
  }
}
