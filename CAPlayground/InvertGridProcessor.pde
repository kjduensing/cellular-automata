class InvertGridProcessor implements GridRulesProcessor {
  Grid processRules(Grid previous) {
    Grid processed = new Grid(width/Cell.SIZE, height/Cell.SIZE);
    
    for (int y = 0; y < grid.gridHeight; y++) {
      for (int x = 0; x < grid.gridWidth; x++) {
        if (previous.getCell(x, y).isOn) {
          processed.getCell(x, y).turnOff();
        } else {
          processed.getCell(x, y).turnOn();
        }
      }
    }
    
    return processed;
  }
}
