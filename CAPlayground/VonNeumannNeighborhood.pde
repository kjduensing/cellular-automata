static class VonNeumannNeighborhood {
  public static Cell[] getNeighbors(Grid grid, Cell cell) {
    Cell[] neighbors = null;
    int wrappedXWest = grid.gridWidth - 1;
    int wrappedXEast = 0;
    int wrappedYNorth = grid.gridHeight - 1;
    int wrappedYSouth = 0;
    
    int x = cell.x/Cell.SIZE;
    int y = cell.y/Cell.SIZE;
    
    // N, E, S, W
    neighbors = new Cell[4];
    
    int northIdx = y - 1;
    
    if (northIdx < 0) {
      northIdx = wrappedYNorth;
    }
    
    int eastIdx = x + 1;
    
    if (eastIdx > grid.gridWidth/Cell.SIZE - 1) {
      eastIdx = wrappedXEast;
    }
    
    int southIdx = y + 1;
    
    if (southIdx > grid.gridHeight/Cell.SIZE - 1) {
      southIdx = wrappedYSouth;
    }
    
    int westIdx = x - 1;
    
    if (westIdx < 0) {
      westIdx = wrappedXWest;
    }
    
    neighbors[0] = grid.getCell(x, northIdx);
    neighbors[2] = grid.getCell(eastIdx, y);
    neighbors[4] = grid.getCell(x, southIdx);
    neighbors[6] = grid.getCell(westIdx, y);
    
    for (Cell c : neighbors) {
      println("(" + c.x/Cell.SIZE + ", " + c.y/Cell.SIZE + ")");
    }
    
    return neighbors;
  }
}
