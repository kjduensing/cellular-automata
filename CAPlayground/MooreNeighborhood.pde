static class MooreNeighborhood {
  public static Cell[] getNeighbors(Grid grid, Cell cell) {
    Cell[] neighbors = null;
    int wrappedXWest = grid.gridWidth - 1;
    int wrappedXEast = 0;
    int wrappedYNorth = grid.gridHeight - 1;
    int wrappedYSouth = 0;
    
    int x = cell.x/Cell.SIZE;
    int y = cell.y/Cell.SIZE;
    
    // N, NE, E, SE, S, SW, W, NW
    neighbors = new Cell[8];
    
    int northIdx = y - 1;
    
    if (northIdx < 0) {
      northIdx = wrappedYNorth;
    }
    
    int eastIdx = x + 1;
    
    if (eastIdx > grid.gridWidth - 1) {
      eastIdx = wrappedXEast;
    }
    
    int southIdx = y + 1;
    
    if (southIdx > grid.gridHeight - 1) {
      southIdx = wrappedYSouth;
    }
    
    int westIdx = x - 1;
    
    if (westIdx < 0) {
      westIdx = wrappedXWest;
    }
    
    neighbors[0] = grid.getCell(x, northIdx);
    neighbors[1] = grid.getCell(eastIdx, northIdx);
    neighbors[2] = grid.getCell(eastIdx, y);
    neighbors[3] = grid.getCell(eastIdx, southIdx);
    neighbors[4] = grid.getCell(x, southIdx);
    neighbors[5] = grid.getCell(westIdx, southIdx);
    neighbors[6] = grid.getCell(westIdx, y);
    neighbors[7] = grid.getCell(westIdx, northIdx);
    
    return neighbors;
  }
}
